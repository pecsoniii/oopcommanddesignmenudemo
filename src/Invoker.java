public class Invoker {
    Command displayMenu;
    Command displayTab;
    Command submitOrder;
    Aggregator agg = new Aggregator();
    Command cmd;

    public Invoker() {
    }

    public void setCommand(Command command) {
        cmd = command;
    }

    public void commandProcess() {
        cmd.execute();
    }


    public void displayMenuCommand(Menu menu) {
        menu = (Menu) agg.aggregate(menu);
        displayMenu = new DisplayMenuCommand(menu);
    }


    public void orderMenuCommand(Orders orders) {
        orders = (Orders) agg.aggregate(orders);
        submitOrder = new SubmitOrderCommand(orders);
    }


    public void displayTabCommand(Tab tab) {
        tab = (Tab) agg.aggregate(tab);
        displayTab = new DisplayTabCommand(tab);
    }
}
