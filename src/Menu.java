import java.util.ArrayList;

public class Menu implements Product{
    private String menuOutput = "";
    private ArrayList<MenuItem> menuItems;

    public Menu(ArrayList<MenuItem> menuItems){
        this.menuItems = menuItems;
    }
    @Override
    public void prtProduct(){
        //Test


        int index = 0;

        System.out.print("************************************\nYour test menu:  \n");

        while(hasNext(index)) {
            menuOutput+="\nDescription:  " + menuItems.get(index).getDescription()+ "\n"+
                    "Price:  " + menuItems.get(index).getCost() + "\n";

            index++;

        }
        System.out.print(menuOutput+"\n*****************************************\n");
    }

    private Boolean hasNext(int index){
        return index<menuItems.size();
    }



}
