import java.util.ArrayList;

public class Orders implements Product{
    private String orderOutput = "";
    private ArrayList<OrderItem> orderItems;

    public Orders (ArrayList<OrderItem> orderItems){this.orderItems=orderItems;}
    @Override
    public void prtProduct(){
        //Test Order
        int index = 0;
        System.out.print("\nSubmitting order...\n");

        while(hasNext(index)){


            orderOutput+="\nOrderNo:  " + orderItems.get(index).getOrderNo()+"\nItemNo:  " +
                    orderItems.get(index).getItemNo()+"\n";
        index++;
        }

        System.out.print(orderOutput+"\n********************************************");

    }

    private Boolean hasNext(int index){
        return index<orderItems.size();
    }

}
