import java.util.ArrayList;

public class Tab implements Product {
    private String output = "";
    private double totalCost = getCost();
    private ArrayList<String> tab = new ArrayList<>();
    private ArrayList<OrderItem> orders;
    private ArrayList<MenuItem> menu;

    public Tab(ArrayList<MenuItem> menu, ArrayList<OrderItem> orders) {

        this.menu = menu;
        this.orders = orders;
    }

    public void prtProduct() {
        int index = 0;
        System.out.print("\n\nYour Tab:  " + "\n");
        prepareTabList();
        while (hasNext(index, tab)) {

            output += tab.get(index);
            //         "Price:  " + menu.get(index).getCost() + "\n";
            index++;
        }
        output += "\n**********************************\n" + "Your total cost:  " + totalCost;
        System.out.print(output);
    }

    private Boolean hasNext(int index, ArrayList<?> arrayList) {
        return index < arrayList.size();
    }

    private void prepareTabList() {
        int index = 0;

        while (hasNext(index, orders)) {
            tab.add(
                    "Description:  "
                            + menu.get(orders.get(index).getItemNo()).getDescription() +
                            "\nPrice  "
                            + menu.get(orders.get(index).getItemNo()).getCost() + "\n\n"
            );
            addToCost(menu.get(orders.get(index).getItemNo()).getCost());
            index++;
        }


    }

    private void addToCost(double cost) {
        totalCost += cost;
    }

    public Double getCost(){return totalCost;}

}
