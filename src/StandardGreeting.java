public class StandardGreeting implements AddOn, Greetings {

    @Override
    public boolean applies(Product p) {
        return true;
    }

    @Override
    public String getLines() {
        return "\nThank you, come again!  ***This message is a Decorator*** :)\n";
    }
}
