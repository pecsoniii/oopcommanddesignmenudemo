public class BuyFourGetOne implements AddOn, Coupon {

    @Override
    public boolean applies(Product items) {
        return true;
    }

    @Override
    public String getLines() {
        return "You bought four get one free!  Here is a coupon for a free dinner!  ***Decorator Message***";
    }
}
