import java.util.ArrayList;

public class Aggregator {


    private Menu menu;
    private Orders orders;
    private Tab tab;

    public Aggregator(){

    }

    public Product aggregate(Product request){

        if(request instanceof Menu){
            return getMenu();
        }
        else if (request instanceof Orders){
            return getOrder();
        }
        else if (request instanceof Tab){
            return getTab();
        }
        else return null;
    }

    private Menu getMenu(){
        this.menu = new Menu(new ArrayList<MenuItem>());
        return menu;
    }
    private Orders getOrder(){
        this.orders = new Orders(new ArrayList<OrderItem>());
        return orders;
    }
    private Tab getTab(){
        this.tab = new Tab(new ArrayList<MenuItem>(),new ArrayList<OrderItem>());
        return tab;
    }

}
