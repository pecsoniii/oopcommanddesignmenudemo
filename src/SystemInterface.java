import java.util.ArrayList;

public class SystemInterface {
    private ArrayList<OrderItem> orderItems = new ArrayList();
    private ArrayList<MenuItem> menuItems = new ArrayList();
    private AddOn a;

    public SystemInterface(ArrayList<OrderItem> orderItems) {

        this.orderItems = orderItems;
        performEntreeProcess();

    }


    private void performEntreeProcess() {
        populateMenu();

        Invoker menu = new Invoker();
        Invoker order = new Invoker();
        Invoker tab = new Invoker();

        Menu thisMenu = new Menu(menuItems);
        Orders orders = new Orders(orderItems);
        Product thisTab = new Tab(menuItems, orderItems);
        ;
        System.out.print(orderItems.size() > 3);
        if (orderItems.size() > 3) {
            thisTab = new TabDecorator(new BuyFourGetOne(), thisTab);
        }
        thisTab = new TabDecorator(new StandardGreeting(), thisTab);
        menu.setCommand(new DisplayMenuCommand(thisMenu));
        order.setCommand(new SubmitOrderCommand(orders));

        tab.setCommand(new DisplayTabCommand(thisTab));


        menu.commandProcess();
        order.commandProcess();
        tab.commandProcess();
    }


    private void populateMenu() {
        MenuItem testitem1 = new MenuItem(0, "Steak", 38.99);
        MenuItem testitem2 = new MenuItem(1, "Chicken", 28.99);
        MenuItem testitem3 = new MenuItem(2, "Pasta", 15.00);
        menuItems.add(testitem1);
        menuItems.add(testitem2);
        menuItems.add(testitem3);
    }
}

