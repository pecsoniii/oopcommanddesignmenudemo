public class MenuItem {
    private int itemNo;
    private String description;
    private double cost;
    public MenuItem(int itemNo, String description, double cost){
        this.itemNo=itemNo;
        this.description=description;
        this.cost=cost;
    }

    public int getItemNo() {
        return itemNo;
    }

    public String getDescription() {
        return description;
    }

    public double getCost() {
        return cost;
    }
}
