public abstract class Decorator implements Product {
    private Product trailer;

    public Decorator(Product p){
        trailer = p;
    }

    protected void callTrailer(){
        trailer.prtProduct();
    }

    public abstract void prtProduct();
}
