public class DisplayTabCommand implements Command {
    private Product tab;

    public DisplayTabCommand(Product tab){
        this.tab = tab;
    }
    @Override
    public void execute() {
        tab.prtProduct();
    }
}
