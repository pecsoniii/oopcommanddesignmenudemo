public class OrderItem {

    private int orderNo, itemNo;
    public OrderItem(int orderNo, int itemNo){
        this.orderNo=orderNo;
        this.itemNo=itemNo;
    }


    public int getOrderNo() {
        return orderNo;
    }



    public int getItemNo() {
        return itemNo;
    }

}
