public class TabDecorator extends Decorator {
    private AddOn a;
    public TabDecorator(AddOn a, Product p) {
        super(p);
        this.a=a;
    }

    @Override
    public void prtProduct() {
        System.out.println(a.getLines());
        callTrailer();
    }
}
