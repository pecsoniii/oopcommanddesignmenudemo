public interface AddOn {
    boolean applies(Product items);
    String getLines();
}
