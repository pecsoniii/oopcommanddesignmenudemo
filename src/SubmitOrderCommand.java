public class SubmitOrderCommand implements Command {

    private Orders order;

    public SubmitOrderCommand (Orders order){
        this.order = order;
    }
    @Override
    public void execute() {
        order.prtProduct();
    }
}
